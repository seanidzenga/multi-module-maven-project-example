module gui {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;
    requires api;
    requires com.google.protobuf;
    requires core;

    opens com.example.multimoduleproject.gui to javafx.fxml;
    exports com.example.multimoduleproject.gui;
}