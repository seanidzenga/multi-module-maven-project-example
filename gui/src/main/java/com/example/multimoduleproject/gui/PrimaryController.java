package com.example.multimoduleproject.gui;

import javafx.fxml.FXML;

import java.io.IOException;

public class PrimaryController {

    @FXML
    private void switchToSecondary() throws IOException {
        App.setRoot("secondary");
    }

    @FXML
    private void generateMessage() {
        App.foobar();
    }
}
