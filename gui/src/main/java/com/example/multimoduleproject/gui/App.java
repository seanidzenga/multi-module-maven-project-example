package com.example.multimoduleproject.gui;

import com.example.multimoduleproject.api.Api;
import com.example.multimoduleproject.api.Bar;
import com.example.multimoduleproject.api.Foo;
import com.example.multimoduleproject.core.Core;
import com.google.protobuf.ByteString;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {
        scene = new Scene(loadFXML("primary"), 640, 480);
        stage.setScene(scene);
        stage.show();
    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {

        launch();
    }

    static void foobar(){

        Api.BarFooMessage.Builder barfooBuilder = Api.BarFooMessage.newBuilder();

        barfooBuilder.setHello("Hallo");
        barfooBuilder.setWorld("Welt");

        Bar.BarMessage.Builder barMessageBuilder = Bar.BarMessage.newBuilder();
        barMessageBuilder.setBigValue(977L);
        barMessageBuilder.setValue(999);
        barfooBuilder.setBar(barMessageBuilder.build());

        Foo.FooMessage.Builder fooMessageBuilder = Foo.FooMessage.newBuilder();
        fooMessageBuilder.setRawData(ByteString.copyFrom("How much wood would a woodchuck chuck if a woodchuck could chuck wood?".getBytes()));
        barfooBuilder.setFoo(fooMessageBuilder.build());

        System.out.println(barfooBuilder.build());
    }

    static void barfoo(){

        Core core = new Core();
        System.out.println(core.buildABarFooMessage());
    }

}