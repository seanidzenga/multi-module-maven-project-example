package com.example.multimoduleproject.gui;

import javafx.fxml.FXML;

import java.io.IOException;

public class SecondaryController {

    @FXML
    private void switchToPrimary() throws IOException {
        App.setRoot("primary");
    }

    @FXML
    private void generateMessage(){
        App.barfoo();
    }
}