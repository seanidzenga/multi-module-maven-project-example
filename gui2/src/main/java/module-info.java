module guitwo {
    requires javafx.controls;
    requires javafx.fxml;

    opens com.example.multimoduleproject.gui2 to javafx.fxml;
    exports com.example.multimoduleproject.gui2;
}