module core {
    requires com.google.protobuf;
    requires api;

    exports com.example.multimoduleproject.core;
}