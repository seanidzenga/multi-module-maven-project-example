package com.example.multimoduleproject.core;

import com.example.multimoduleproject.api.Api;
import com.example.multimoduleproject.api.Bar;
import com.example.multimoduleproject.api.Foo;
import com.google.protobuf.ByteString;

public class Core {

    public Core(){
        System.out.println("Constructed an instance of Core!");
    }

    public Api.BarFooMessage buildABarFooMessage(){

        Api.BarFooMessage.Builder barfooBuilder = Api.BarFooMessage.newBuilder();

        barfooBuilder.setHello("Bonjour");
        barfooBuilder.setWorld("le monde");

        Bar.BarMessage.Builder barMessageBuilder = Bar.BarMessage.newBuilder();
        barMessageBuilder.setBigValue(100L);
        barMessageBuilder.setValue(666);
        barfooBuilder.setBar(barMessageBuilder.build());

        Foo.FooMessage.Builder fooMessageBuilder = Foo.FooMessage.newBuilder();
        fooMessageBuilder.setRawData(ByteString.copyFrom("She sells seashells down by the seashore".getBytes()));
        barfooBuilder.setFoo(fooMessageBuilder.build());

        return barfooBuilder.build();
    }
}
