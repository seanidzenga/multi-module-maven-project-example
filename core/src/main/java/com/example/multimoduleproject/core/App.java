package com.example.multimoduleproject.core;

/* Because the protobuf api is set to use the same package as this class
* there's no need to use an import statement */

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ){

        System.out.println( "Hello World!" );
    }
}
