package com.example.multimoduleproject.example;

public class Main {

    public static void main(String[] args){

        if(args.length > 0){

            switch(args[0]){
                case "gui1":
                    com.example.multimoduleproject.gui.App.main(args);
                    break;
                case "gui2":
                    com.example.multimoduleproject.gui2.App.main(args);
                    break;
                default:
                    System.out.println("gui does not exist");
                    break;
            }

        }else{

            System.out.println("No GUI specified!");
        }
    }
}
