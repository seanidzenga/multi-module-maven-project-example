# multi-module maven project example

An example of a multi-module maven project, a proof-of-concept which will feature an MVVM implementation for multiple UIs with a core component as well as a protobuf package being built for multiple languages.

## Project Structure

This project is split into 5 modules, api, core, example, gui and gui2

* **api** - demonstrates how a protocol buffers api could be included as a separate module in a maven project, allowing a separate jar to be produced and imported into other java projects. This module has also been set up to produce C++ code under `target/generated-sources/protobuf/cpp`

* **core** - this module would act as the main bread-and-butter of the application, holding the backend logic and classes.

* **gui, gui2** - two javafx GUI representations for the application, they don't do anything fascinating but can demonstrate how the view logic can be effectively separated from the rest of the application, allowing for many different ways to represent the data from the **core** module. In a MVVM implementation these modules would contain both the view and view model, whereas the model would be contained within **core**.

* **example** - this module is more or less the final distributable, it only has a single Main class which is used to launch the appropriate javafx frontend via commandline arguments i.e. `example gui1` and `example gui2`. This module's `pom` does the hard work of building an app image as well, using the moditect maven plugin we're able to inject a `module-info.class` for dependencies that haven't been updated to include them (in this case protobuf) as well as build the app image itself via jlink (moditect takes care of both for us)

## How to build

Simply running `mvn clean install` from the root directory will work through all goals and produce an app image at `example/target/dist`

* 2020-12-14 Note that you should always run clean as the moditect plugin currently has a bug that won't allow it to overwrite directories that already exist (see: https://github.com/moditect/moditect/issues/93)

## Running

Set your main class to `com.example.multimoduleproject.example.Main` and add a command line argument as either `gui1` or `gui2`